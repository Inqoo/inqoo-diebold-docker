
# MV DNAcademy
C:/Projects -> Z 

<!-- bash -->
cd /z/
npm init -y

npm i json-server

docker run 
-it
-p 3000:3000 
-v "//$(pwd)/://home" 
node 
npx json-server -H 0.0.0.0 --static . ./db.json

chrome -> otwórz http://adres-maszyny-dockera:3000/

# default  VM
C:/Projects -> Z 

ls /z/ 
<!-- powinnismy widziec pliki C:/Projects -->

<!-- aadres-maszyny-dockera -->
ifconfig eth1
192.268.??.??



## Docker run

docker run 
-i -t 
--rm 
-p 3000:3000 

-v "//$(pwd)/://home/"  

-w "//home" 

node 

npm start

path C:\Projects
name c/Projects
autoMount yes


## Toutorial

docker run --name inqoo -p 80:80 docker/getting-started
192.168.99.100


# docker build ./ -t my-app
# docekr push mojekonto/my-app
# docker run -it -p --rm 3000:3000 my-app

# /etc/todos/todo.db
# docker volume create todo-db
# docker run -it -p   3000:3000  --rm -v todo-db:/etc/todos my-app

# docker run -it -p   3000:3000  --rm -v todo-db:/etc/todos -v "/$(pwd):/app" my-app

# docker network create todo-app


docker run -d \
    --name mysql \
    --network todo-app --network-alias mysql \
    -v todo-mysql-data:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=secret \
    -e MYSQL_DATABASE=todos \
    mysql:5.7

docker exec -it mysql mysql -p

docker run -dp 3000:3000 \
  -w "//app" -v "//$(pwd)/:/app" \
  --network todo-app \
  -e MYSQL_HOST=mysql \
  -e MYSQL_USER=root \
  -e MYSQL_PASSWORD=secret \
  -e MYSQL_DB=todos \
  node:12-alpine \
  sh -c "yarn install && yarn run dev"

